# Licenses
Concept licenses for potential use in my present and future works.

This repository contains:
- [03utils License](https://codeberg.org/chris03-dev/licenses/src/branch/main/03utils-lc.txt)
- [NCPB License](https://codeberg.org/chris03-dev/licenses/src/branch/main/noprob-lc.txt)